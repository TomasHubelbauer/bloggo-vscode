# VS Code

- [ ] Check out [VSCode extension samples from Microsoft](https://github.com/Microsoft/vscode-extension-samples)
- [ ] Check out [VS Code can do that?](https://vscodecandothat.com/)
- [ ] Check out https://code.visualstudio.com/docs/editor/extension-gallery#_workspace-recommended-extensions for my projects

## Recipes

[Microsoft VS Code recipes](https://github.com/Microsoft/vscode-recipes)

- [ ] Contribute *Debugging Rust apps*

## My Extensions

[Tomas Hubelbauer](https://marketplace.visualstudio.com/publishers/TomasHubelbauer)

- [ ] Figure out a good solution for telemetry
    - Anonymize reports (replace all alphanum character with `a` or `0` and send no PII)
    - Introduce a switch for telemetry, by default ask for eveyr report to be sent, can switch to never or always
- [ ] Follow [*TreeDataProvider: allow selecting a TreeItem without affecting its collapsibleState*](https://github.com/Microsoft/vscode/issues/34130)

[Commands](https://code.visualstudio.com/docs/extensionAPI/vscode-api-commands)

[Statistics](https://gitlab.com/TomasHubelbauer/vscode-publisher-extensions-statistics)

### [Box Drawing](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.vscode-box-drawing) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.vscode-box-drawing.svg)

Draws boxes using ASCII or Unicode box drawing characters by enclosing mouse selection in Visual Studio Code.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-box-drawing)

### [MarkDown Todo](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.vscode-markdown-todo) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.vscode-markdown-todo.svg)

Collects MarkDown checkboxen in real time in a *To-Do* Output pane channel.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-markdown-todo)

### [Week Number](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.vscode-week-number) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.vscode-week-number.svg)

Displays the current week number in the status bar.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-week-number)

### [MarkDown Link Suggestions](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.vscode-markdown-link-suggestions) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.vscode-markdown-link-suggestions.svg)

Suggests workspace files as well as MarkDown file heading anchors when typing the target portion (`[](this)`) of a MarkDown link.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-markdown-link-suggestions)

### [MarkDown Email Links](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.vscode-markdown-email-links) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.vscode-markdown-email-links.svg)

Highlights email addresses in MarkDown and makes them clickable `mailto` links.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-markdown-email-link)

### [MarkDown Table Format](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.vscode-markdown-table-format) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.vscode-markdown-table-format.svg)

Formats tables in MarkDown documents using the *Format document* VS Code context menu option.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-markdown-table-format)

### [MarkDown Jira Links](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.markdown-jira-links) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.markdown-jira-links.svg)

Highlights configured Jira ticket codes in MarkDown files and makes them clickable.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-markdown-jira-links)

### [MarkDown Table of Contents](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.markdown-table-of-contents) ![Installs](https://vsmarketplacebadge.apphb.com/installs-short/TomasHubelbauer.markdown-table-of-contents.svg)

Displays CodeLens items atop MarkDown headers providing a back to top action on non-title headers and a header jump quick pick on the title header.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-markdown-table-of-contents)

## Checklist

A checklist of creating and maintaining extensions.

- Run `yo code` and fill in the human readable name (without the *VS Code* prefix) first and the extension code second
- Fill in the `keywords` in `package.json`
- Fill in the `description` in `package.json`
- Fill in the `categories` in `package.json`
- Create a screenshot using ScreenToGit and link it from the README
- Fill in `repository` in `package.json` for the screenshot image to work
- Create a `demo` directory where files are prepared for showcasing features in the screenshot
- Create an icon (a visual detail from the extension's main functionality)
- Set up tests and Travis CI
- Add to My VS Code extensions above
- Display Travis CI status in the README
